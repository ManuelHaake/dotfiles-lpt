# -*- coding: utf-8 -*-
import os
import re
import socket
import subprocess
from libqtile import qtile, layout, bar, widget, hook
from libqtile.config import Click, Drag, Group, KeyChord, Key, Match, Screen, ScratchPad, DropDown
from libqtile.command import lazy
from libqtile.lazy import lazy
from typing import List  # noqa: F401
import custom_widgets

mod = "mod4"                                     # Sets mod key to SUPER/WINDOWS
myTerm = "alacritty"                             # My terminal of choice
alt = "mod1"

keys = [
         Key([mod], "Tab",
             lazy.next_layout(),
             desc='Toggle through layouts'
             ),
         Key([mod, "shift"], "r",
             lazy.restart(),
             desc='Restart Qtile'
             ),
         ### Switch focus of monitors
         Key([alt], "period",
             lazy.next_screen(),
             desc='Move focus to next monitor'
             ),
         Key([alt], "comma",
             lazy.prev_screen(),
             desc='Move focus to prev monitor'
             ),
         ### Switch to Group
         Key([mod], "period",
             lazy.screen.next_group(skip_empty=False),
             desc='Move focus to next group'
             ),
         Key([mod], "comma",
             lazy.screen.prev_group(skip_empty=False),
             desc='Move focus to prev monitor'
             ),
         Key([mod, "shift"], "period",
             lazy.screen.next_group(skip_empty=True),
             desc='Move focus to next group'
             ),
         Key([mod, "shift"], "comma",
             lazy.screen.prev_group(skip_empty=True),
             desc='Move focus to prev monitor'
             ),
         ### Treetab controls
         Key([mod, "shift"], "k",
             lazy.layout.section_up(),
             desc='Move up a section in treetab'
             ),
         Key([mod, "shift"], "j",
            lazy.layout.section_down(),
             desc='Move down a section in treetab'
             ),
         ### Window controls
         Key([mod], "k",
             lazy.layout.down(),
             desc='Move focus down in current stack pane'
             ),
         Key([mod], "j",
             lazy.layout.up(),
             desc='Move focus up in current stack pane'
             ),
         Key([mod], "l",
             lazy.layout.shuffle_down(),
             desc='Move windows down in current stack'
             ),
         Key([mod], "h",
             lazy.layout.shuffle_up(),
             desc='Move windows up in current stack'
             ),
         Key([mod, alt], "l",
             lazy.layout.grow(),
             lazy.layout.increase_nmaster(),
             desc='Expand window (MonadTall), increase number in master pane (Tile)'
             ),
         Key([mod, alt], "h",
             lazy.layout.shrink(),
             lazy.layout.decrease_nmaster(),
             desc='Shrink window (MonadTall), decrease number in master pane (Tile)'
             ),
         Key([mod], "n",
             lazy.layout.normalize(),
             desc='normalize window size ratios'
             ),
         Key([mod, "shift"], "f",
             lazy.window.toggle_floating(),
             desc='toggle floating'
             ),
         Key([mod], "f",
             lazy.window.toggle_fullscreen(),
             desc='toggle fullscreen'
             ),
         ### Stack controls
         Key([mod, "shift"], "space",
             lazy.layout.rotate(),
             lazy.layout.flip(),
             desc='Switch which side main pane occupies (XmonadTall)'
             ),
         Key([mod, "shift"], "Return",
             lazy.layout.toggle_split(),
             desc='Toggle between split and unsplit sides of stack'
             ),
         Key([alt], "j", lazy.spawn("brightnessctl set 10%-")),
	     Key([alt], "k", lazy.spawn("brightnessctl set 10%+")),
         Key([mod,"control"], "Return", lazy.group['scratchpad'].dropdown_toggle('terminal')),
         Key([mod,"control"], "p", lazy.group['scratchpad'].dropdown_toggle('keepassxc')),
]

workspaces = [
    {"name": "var", "key": "1"},
    {"name": "www", "key": "2", "matches": [Match(wm_class='firefox'),Match(wm_class='LibreWolf'),Match(wm_class='qutebrowser'),Match(wm_class='Tor Browser'),Match(role='browser'),Match(wm_class='Firefox')]},
    {"name": "sys", "key": "3", "matches": [Match(wm_class='geany'),Match(wm_class='vim')]},
    {"name": "dev", "key": "4", "matches": [Match(wm_class='pycharm')]},
    {"name": "vrt", "key": "5", "matches": [Match(wm_class='VirtualBox Machine'), Match(wm_class='VirtualBox Manager')]},
    {"name": "ssh", "key": "6", "matches": [Match(title='ssh')]},
    {"name": "doc", "key": "7", "matches": [Match(wm_class='libreoffice')]},
    {"name": "mus", "key": "8"},
    {"name": "vid", "key": "9", "matches": [Match(wm_class='vlc'),Match(wm_class='mpv'),Match(wm_class='FreeTube'),Match(wm_class='Kodi')]},
]

# define scratchpads
groups = [
    ScratchPad("scratchpad", [
        DropDown("terminal", "alacritty --hold -e zsh", opacity=0.98, x=0.15, y=0.05, width=0.7, height=0.7, on_focus_lost_hide=True),
        DropDown("keepassxc", "keepassxc", x=0.2, y=0.25, width=0.5, height=0.6, opacity=1, on_focus_lost_hide=True)]),
    ]

# add workspaces
for workspace in workspaces:
    matches = workspace["matches"] if "matches" in workspace else None
    groups.append(Group(workspace["name"], matches=matches, layout="monadtall"))
    keys.append(Key([mod], workspace["key"], lazy.group[workspace["name"]].toscreen()))
    keys.append(Key([mod, "shift"], workspace["key"], lazy.window.togroup(workspace["name"])))
    keys.append(Key([mod, "control"], workspace["key"], lazy.window.togroup(workspace["name"]), lazy.group[workspace["name"]].toscreen()))


layout_theme = {"border_width": 3,
                "margin": 8,
                "border_focus": "D55FDE",
                "border_normal": "1D2330"
                }

layouts = [
    #layout.MonadWide(**layout_theme),
    #layout.Bsp(**layout_theme),
    #layout.Stack(stacks=2, **layout_theme),
    #layout.Columns(**layout_theme),
    #layout.RatioTile(**layout_theme),
    #layout.VerticalTile(**layout_theme),
    #layout.Matrix(**layout_theme),
    #layout.Zoomy(**layout_theme),
    layout.MonadTall(**layout_theme),
    layout.Max(**layout_theme),
    #layout.Tile(shift_windows=True, **layout_theme),
    #layout.Stack(num_stacks=2),
    #layout.TreeTab(),
    layout.Floating(**layout_theme)
]

colors = [["#282c34", "#282c34"], # Background
	  ["#FFFFFF", "#FFFFFF"], # 1 Foreground
	  ["#282c34", "#282c34"], # 2 Grey Colour
	  ["#E35374", "#E35374"], # 3
	  ["#89CA78", "#89CA78"], # 4 Network
	  ["#ff8200", "#ff8200"], # 5 Volume
	  ["#61AFEF", "#61AFEF"], # 6 Memory
	  ["#b8b51a", "#b8b51a"], # 7 cpu
	  ["#c93a42", "#c93a42"], # 8 clock
	  ["#D55FDE", "#D55FDE"], # 9 updates
      ["#FFFFFF", "#FFFFFF"], # 10 white
      ["#3fff55", "#3fff55"], # 11 
      ["#000000", "#000000"],] # 12 black 


##### DEFAULT WIDGET SETTINGS #####
widget_defaults = dict(
    font="Ubuntu Mono",
    fontsize = 14,
    padding = 2,
    background=colors[0],
    foreground=colors[6]
)
extension_defaults = widget_defaults.copy()

def init_widgets_list():
    widgets_list = [
              widget.Sep(
                       linewidth = 0,
                       padding = 6,
                       foreground = colors[2],
                       background = colors[0]
                       ),
              widget.GroupBox(
                       active = colors[4],
                       inactive = colors[6],
                       other_current_screen_border = colors[5],
                       other_screen_border = colors[2],
                       this_current_screen_border = colors[9],
                       this_screen_border = colors[2],
                       urgent_border = colors[3],
                       urgent_text = colors[3],
                       disable_drag = True,
                       highlight_method = 'text',
                       invert_mouse_wheel = True,
                       margin = 2,
                       padding = 0,
                       rounded = True,
                       urgent_alert_method = 'text'
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[6],
                       background = colors[0]
                       ),
              widget.CurrentLayoutIcon(
                       custom_icon_paths = [os.path.expanduser("~/.config/qtile/icons")],
                       foreground = colors[3],
                       background = colors[0],
                       padding = 0,
                       scale = 0.7
                       ),
              widget.CurrentLayout(
                       foreground = colors[9],
                       background = colors[0],
                       padding = 5
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 20,
                       foreground = colors[6],
                       background = colors[0]
                       ),
              widget.WindowName(
                       foreground = colors[9],
                       background = colors[0],
                       padding = 0
                       ),
              widget.Systray(
                       background = colors[0],
                       padding = 5
                       ),              
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       foreground = colors[0],
                       background = colors[0]
                       ),
              widget.Battery(
                       battery = 0,
                       foreground = colors[9],
                       background = colors[0],
                       charge_char = '^',
		               discharge_char = '',
		               empty_char = '',
		               unknown_char = '',
                       full_char = 'F',
                       show_short_text = False,
                       format = '{char} {percent:2.0%}'
                       ),
              widget.TextBox(
                       text = "|",
                       padding = 1,
                       foreground = colors[9],
                       background = colors[0],
                       fontsize = 14
                       ),
              widget.Battery(
                       battery = 1,
                       foreground = colors[9],
                       background = colors[0],
                       charge_char = '^',
		               discharge_char = '',
                       full_char = 'F',
		               empty_char = '',
		               unknown_char = '',
                       show_short_text = False,
                       format = '{char} {percent:2.0%}'
                       ),
              widget.TextBox(
                       text = " ⟳",
                       padding = 2,
                       foreground = colors[7],
                       background = colors[0],
                       fontsize = 14
                       ),
             widget.CheckUpdates(
                       foreground = colors[7],
                       background = colors[0],
                       update_interval = 1800,
                       colour_have_updates = colors[7],
                       colour_no_updates = colors[7],
                       distro = "Arch_checkupdates",
                       display_format = "{updates}",
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e sudo pacman -Syu')}
                       ),
              widget.TextBox(
                       text = " ﬙",
                       foreground = colors[6],
                       background = colors[0],
                       padding = 0,
                       fontsize = 14
                       ),
              widget.Memory(
                       foreground = colors[6],
                       background = colors[0],
                       mouse_callbacks = {'Button1': lambda: qtile.cmd_spawn(myTerm + ' -e htop')},
                       padding = 3,
                       update_interval = 2.5
                       ),
              custom_widgets.MBNet(
                       format = '{down}↓↑{up}',
                       foreground = colors[4],
                       background = colors[0],
                       update_interval = 1
                       ),
              widget.Sep(
                       linewidth = 0,
                       padding = 10,
                       background = colors[0],
                       foreground = colors[5]
                        ),
              widget.TextBox(
                        background = colors[0],
                        foreground = colors[5],
                        mouse_callbacks = ({
                            "Button1": lambda: qtile.cmd_spawn("amixer -M set Master toggle"),
                            #"Button3": lambda: qtile.cmd_spawn("allacrity -e ~/Dokumente/pulsemixer/pulsemixer"),
                            "Button4": lambda: qtile.cmd_spawn("amixer -M set Master 5%+ unmute"),
                            "Button5": lambda: qtile.cmd_spawn("amixer -M set Master 5%- unmute"),
                        }),
                        padding = 0,
                        text = '墳 ',
                        ),
              widget.Volume(
                        background = colors[0],
                        foreground = colors[5],
                        #mouse_callbacks = {"Button3": lambda: qtile.cmd_spawn("alacritty -e ~/Dokumente/pulsemixer/pulsemixer")},
                        step = 5,
                        ),
              widget.Sep(
                       linewidth = 0,
                       padding = 5,
                       foreground = colors[6],
                       background = colors[0]
                       ),
              widget.Clock(
                       foreground = colors[8],
                       background = colors[0],
                       format = '| %a %b %d  %H:%M '
                       ),
              ]
    return widgets_list


def init_widgets_screen1():
    widgets_screen1 = init_widgets_list()
    return widgets_screen1                       # Slicing removes unwanted widgets on Monitors 1,3

def init_widgets_screen2():
    widgets_screen2 = init_widgets_list()
    return widgets_screen2                       # Monitor 2 will display all widgets in widgets_list

def init_screens():
    return [Screen(top=bar.Bar(widgets=init_widgets_screen1(), opacity=1.0, size=23)),
            Screen(top=bar.Bar(widgets=init_widgets_screen2(), opacity=1.0, size=23))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widgets_screen1 = init_widgets_screen1()
    widgets_screen2 = init_widgets_screen2()


mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(**layout_theme, float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    # default_float_rules include: utility, notification, toolbar, splash, dialog,
    # file_progress, confirm, download and error.
    *layout.Floating.default_float_rules,
    Match(title='Confirmation'),      # tastyworks exit box
    Match(title='Qalculate!'),        # qalculate-gtk
    Match(wm_class='kdenlive'),       # kdenlive
    Match(wm_class='pinentry-gtk-2'), # GPG key password entry
    Match(title='pinentry'),          # GPG key password entry
    Match(title='audiofloat'),        # pulsemixer
    Match(title='calfloat'),        # pulsemixer
    Match(title='weatherfloat'),        # pulsemixer
    Match(wm_class='VirtualBox Machine'),        # VirtualBox
    Match(wm_class='VirtualBox Manager'),        # VirtualBox
    Match(wm_class='Yad'),        # VirtualBox
    Match(title='pulsemixer'),        # pulsemixer
    Match(title='conky')              # GPG key password entry
])

auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~')
    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.client_new
def func(c):
    if c.name == "weatherfloat":
        c.cmd_resize_floating(470, 260)

@hook.subscribe.client_managed
def go_to_group(win):
    win.group.cmd_toscreen(toggle=False)

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
