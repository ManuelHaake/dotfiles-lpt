#!/bin/bash
#enables fn2-key on keychron keyboard as actual trigger for F-keys
#
#
#   line for roots crontab:
#   @reboot /bin/bash -c 'echo 0 | sudo tee /sys/module/hid_apple/parameters/fnmode'

#
#
echo 0 | sudo tee /sys/module/hid_apple/parameters/fnmode
