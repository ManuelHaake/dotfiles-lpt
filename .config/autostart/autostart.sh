#!/bin/bash

#turn off touchpad
~/.local/bin/touchpad_off &

# start sxhkd
/usr/bin/sxhkd &

# remap Capslock to Esc
setxkbmap -option 'caps:escape' &

# set wallpapers
/usr/bin/variety --profile /home/locke/.config/variety/ &

# start conky
sleep 5 && ~/.config/conky/start_concyrc_1

