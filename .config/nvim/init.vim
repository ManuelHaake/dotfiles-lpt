let mapleader =" "

call plug#begin('~/.vim/plugged')
Plug 'preservim/nerdtree'
Plug 'tpope/vim-commentary'
Plug 'octol/vim-cpp-enhanced-highlight'
" Plug 'vim-python/python-syntax'
call plug#end()

set bg=light
set go=a
set mouse=a
set nohlsearch
set clipboard+=unnamedplus
set incsearch
set smartindent

set undodir=~/.vim/undodir
set undofile
set noswapfile
set nobackup
set wrap
set wrapmargin=0

set tabstop=8 softtabstop=0 expandtab shiftwidth=4 smarttab

map <F6> :setlocal spell! spelllang=en_us,de_de<CR>

" Some basics:
nnoremap c "_c
set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber
" Enable autocompletion:
set wildmode=longest,list,full
" Disables automatic commenting on newline:
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Splits open at the bottom and right, which is non-retarded, unlike vim defaults.
set splitbelow splitright

" Nerd tree
map <leader>n :NERDTreeToggle<CR>
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
if has('nvim')
    let NERDTreeBookmarksFile = stdpath('data') . '/NERDTreeBookmarks'
else
    let NERDTreeBookmarksFile = '~/.vim' . '/NERDTreeBookmarks'
endif

" Shortcutting split navigation, saving a keypress:
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

" Copy to clipboard
vnoremap  <C-c>  "+y
nnoremap  <C-c>  "+y

" Paste from clipboard
nnoremap <C-v> "+p
vnoremap <C-v> "+p

" Safe and Exit
nmap <leader>w :w!<cr>
nmap <leader>q :q<cr>   

" Inser a new line under/above the curser without leaving normal mode
map <Enter> o<ESC>
map <S-Enter> O<ESC>

" Select all
nnoremap <leader>a ggVG

" Allow saving of files as sudo when I forgot to start vim using sudo.
cnoremap w!! execute 'silent! write !SUDO_ASKPASS=`which ssh-askpass` sudo tee % >/dev/null' <bar> edit!

" let g:python_highlight_all = 1

