## Tor-Browser Profile [compatible 8.x]
## Mike Kuketz 
## 06.09.2018 
## Version 1.02

## Disable access 
# Disable access to common system management tools (sudo, mount, etc.) 
include /etc/firejail/disable-common.inc 
# Disable access to common program configs in ${HOME} 
include /etc/firejail/disable-programs.inc 
# Disable access to common password manager files 
include /etc/firejail/disable-passwdmgr.inc 
# Disable access to development tools 
include /etc/firejail/disable-devel.inc

noblacklist ${HOME}/firefox-portable
whitelist ${HOME}/firefox-portable

## Security filters 
# Blacklist all Linux capabilities 
caps.drop all 
# Ensures that child processes cannot acquire new privileges 
nonewprivs 
# No root account. Only one user, the current one 
noroot 
# Disable supplementary groups 
nogroups
# Protocol filter for unix sockets and IPv4/IPv6 
protocol unix,inet,inet6 
# Run the program directly, without a user shell 
shell none 
# Enable default seccomp filter and blacklist the syscalls 
seccomp

## Filesystem
# Mount an empty temporary filesystem on top of /tmp directory 
private-tmp 
# Create a new /dev directory
private-dev 
# Build new /etc in a temporary filesystem (gets discarded).
private-etc empty 
# Build new /bin in a temporary filesystem. Copy the programs in the list.
private-bin bash,sh,grep,tail,env,gpg,id,readlink,dirname,test,mkdir,ln,sed,cp,rm,getconf 
# Use directory as user home 
##private ~/Dokumente/tor-browser_en-US
# Blacklist 
blacklist /boot 
blacklist /mnt 
blacklist /media 
blacklist /root 
blacklist /srv

whitelist ~/firefox-portable/firefox
whitelist ~/firefox-portable/profilordner

## Networking 
# Default network filter for new created network namespace 
netfilter
